package com.example.android.agameofquestions;

import android.content.Context;
import android.content.res.Resources;

import java.util.Arrays;

/**
 * Created by Marco on 29.01.2018.
 */

public class DiabloQuestions extends Questions {

    private Context context;

    public DiabloQuestions(Context con) {
        this.context = con;
        Settings.resToUse = R.drawable.diablo;
        addQuestions();
    }

    private void addQuestions() {
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.diablo_question_one)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_one)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_one)[2], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_one)[3], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_one)[4], true)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.diablo_question_two)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_two)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_two)[2], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_two)[3], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_two)[4], false)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.diablo_question_three)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_three)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_three)[2], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_three)[3], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_three)[4], false)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.diablo_question_four)[0],
                true,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_four)[1], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_four)[2], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_four)[3], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_four)[4], true)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.diablo_question_five)[0],
                true,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_five)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_five)[2], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_five)[3], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_five)[4], false)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.diablo_question_six)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_six)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_six)[2], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_six)[3], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.diablo_question_six)[4], false)
                )));
    }
}
