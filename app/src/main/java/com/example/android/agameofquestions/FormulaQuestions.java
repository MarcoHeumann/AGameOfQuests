package com.example.android.agameofquestions;

import android.content.Context;

import java.util.Arrays;

/**
 * Created by Marco on 29.01.2018.
 */

class FormulaQuestions extends Questions {

    private Context context;

    FormulaQuestions(Context con) {
        this.context = con;
        Settings.resToUse = R.drawable.f1;
        addQuestions();
    }

    private void addQuestions() {
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.formula_question_one)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_one)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_one)[2], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_one)[3], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_one)[4], false)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.formula_question_two)[0],
                true,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_two)[1], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_two)[2], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_two)[3], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_two)[4], false)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.formula_question_three)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_three)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_three)[2], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_three)[3], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_three)[4], false)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.formula_question_four)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_four)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_four)[2], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_four)[3], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_four)[4], true)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.formula_question_five)[0],
                false,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_five)[1], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_five)[2], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_five)[3], false),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_five)[4], false)
                )));
        questList.add(new QuestionItem(context.getResources().getStringArray(R.array.formula_question_six)[0],
                true,
                Arrays.asList(
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_six)[1], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_six)[2], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_six)[3], true),
                        new AnswerItem(context.getResources().getStringArray(R.array.formula_question_six)[4], true)
                )));
    }
}
