package com.example.android.agameofquestions;

/**
 * Created by Marco on 28.01.2018.
 */

abstract class Settings {
    static String userName = "";
    static String quizName = "";
    static boolean isFormula = false;
    static int resToUse;
    static boolean isDarkTheme = true;
}
