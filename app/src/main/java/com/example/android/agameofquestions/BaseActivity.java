package com.example.android.agameofquestions;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    // Welcome references
    private ImageView welcomeImage;
    private Questions questItems = new Questions();
    private int questIndex;
    private String historyData;

    // Quiz references
    private ImageView questionImage;
    private TextView questionView;
    private LinearLayout layoutMulti;
    private CheckBox checkOne;
    private CheckBox checkTwo;
    private CheckBox checkThree;
    private CheckBox checkFour;
    private RadioGroup layoutSingle;
    private RadioButton radioOne;
    private RadioButton radioTwo;
    private RadioButton radioThree;
    private RadioButton radioFour;

    private ImageButton callerButton;
    private ImageButton passOnButton;
    private ImageButton removeTwoButton;

    // Result references
    private ImageView resultImage;
    private TextView resultQuizName;
    private TextView resultScore;
    private TextView resultMessage;

    // Highscore references
    private TextView highscoreText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // set theme
        setTheme(Settings.isDarkTheme ? R.style.AppThemeDark : R.style.AppThemeLight);

        // set layout
        setContentView(R.layout.welcome);

        // set default values for the layout
        setDefaults();
        super.onCreate(savedInstanceState);
    }

    /**
     * Sets default values on the layout view to the defaults (duh!) or whatever the user entered before.
     */
    private void setDefaults() {
        EditText userNameInput = findViewById(R.id.user_name_input);
        userNameInput.setText(Settings.userName);
        historyData = readData();

        welcomeImage = findViewById(R.id.welcome_image);

        userNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Settings.userName = s.toString();
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        if (Settings.isFormula) {
            RadioButton rbFormula = findViewById(R.id.radio_formula);
            rbFormula.setChecked(Settings.isFormula);

        } else {
            RadioButton rbDiablo = findViewById(R.id.radio_diablo);
            rbDiablo.setChecked(!Settings.isFormula);
        }

        Switch switchButton = findViewById(R.id.switch_theme_toggle);
        switchButton.setChecked(Settings.isDarkTheme);

    }

    /**
     * Sets references used in the Quiz part of the app
     */
    private void setQuizReferences() {
        questionImage = findViewById(R.id.quiz_image);
        questionImage.setImageResource(Settings.resToUse);
        questionView = findViewById(R.id.text_view_question);
        layoutMulti = findViewById(R.id.check_group);
        layoutSingle = findViewById(R.id.radio_group);
        checkOne = findViewById(R.id.check_answer_one);
        checkTwo = findViewById(R.id.check_answer_two);
        checkThree = findViewById(R.id.check_answer_three);
        checkFour = findViewById(R.id.check_answer_four);
        radioOne = findViewById(R.id.radio_answer_one);
        radioTwo = findViewById(R.id.radio_answer_two);
        radioThree = findViewById(R.id.radio_answer_three);
        radioFour = findViewById(R.id.radio_answer_four);

        callerButton = findViewById(R.id.quiz_call_button);
        callerButton.setVisibility(View.VISIBLE);
        passOnButton = findViewById(R.id.quiz_pass_button);
        passOnButton.setVisibility(View.VISIBLE);
        removeTwoButton = findViewById(R.id.quiz_remove_button);
        removeTwoButton.setVisibility(View.VISIBLE);
    }

    private void setHighscorePreferences() {
        highscoreText = findViewById(R.id.highscore_data_view);
        highscoreText.setText(historyData);
    }

    /**
     * sets references used in the Result part of the app
     */
    private void setResultReferences() {
        resultImage = findViewById(R.id.result_image);
        resultQuizName = findViewById(R.id.resultQuizType);
        resultScore = findViewById(R.id.resultScore);
        resultMessage = findViewById(R.id.resultMessage);

        resultImage.setImageResource(Settings.resToUse);
        resultQuizName.setText(Settings.quizName);
        resultScore.setText(String.format("%s %s %s", getString(R.string.you_scored), calculateResults(), "%"));
        resultMessage.setText(String.format(getString(R.string.congrats_text), Settings.userName, getMessageText()));
    }

    /**
     * Handles the selection changes, so the correct quiz is being used.
     *
     * @param view View triggering this method
     */
    public void onQuizSelectionClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_formula:
                if (checked) {
                    Settings.isFormula = true;
                }
                break;
            case R.id.radio_diablo:
                if (checked) {
                    Settings.isFormula = false;
                }
                break;
        }
    }

    /**
     * Switches themes between dark and light.
     * Calls setContentView again to show the changes and setDefaults to keep user input.
     *
     * @param view View triggering this method
     */
    public void switchTheme(View view) {

        Settings.isDarkTheme = ((Switch) view).isChecked();

        setTheme(Settings.isDarkTheme ? R.style.AppThemeDark : R.style.AppThemeLight);
        setContentView(R.layout.welcome);
        setDefaults();
    }

    /**
     * Displays the Welcome screen via the main activity.
     *
     * @param view View triggering this method
     */
    public void showWelcome(View view) {
        setContentView(R.layout.welcome);
        setDefaults();
        if (questItems.questList.size() > 0) {
            questItems.questList.clear();
        }
        questIndex = 0;
    }

    /**
     * Displays the Quiz via the main activity
     *
     * @param view View triggering this method
     */
    public void showQuiz(View view) {
        setContentView(R.layout.quiz);
        if (Settings.isFormula) {
            questItems = new FormulaQuestions(this);
            Settings.quizName = getString(R.string.formula_1);
        } else {
            questItems = new DiabloQuestions(this);
            Settings.quizName = getString(R.string.diablo_universe);
        }
        setQuizReferences();
        setActiveQuestion(questIndex, false);
    }

    /**
     * Displays the next question or the result screen via the main activity
     *
     * @param view View triggering this method
     */
    public void goAhead(View view) {
        checkAnswer();
        questIndex++;
        if (questIndex == questItems.questList.size()) {
            setContentView(R.layout.results);
            setResultReferences();
            saveData();
        } else {
            setActiveQuestion(questIndex, false);
        }
    }

    /**
     * checks if the answer given is correct. This should be done via data binding to make it cleaner.
     */
    private void checkAnswer() {
        QuestionItem qi = questItems.questList.get(questIndex);
        qi.answeredCorrectly = false;
        if (qi.isMulti) {
            if (qi.listAnswers.get(0).isTrue == checkOne.isChecked() &&
                    qi.listAnswers.get(1).isTrue == checkTwo.isChecked() &&
                    qi.listAnswers.get(2).isTrue == checkThree.isChecked() &&
                    qi.listAnswers.get(3).isTrue == checkFour.isChecked()) {
                qi.answeredCorrectly = true;
            }
        } else {
            if (qi.listAnswers.get(0).isTrue == radioOne.isChecked() &&
                    qi.listAnswers.get(1).isTrue == radioTwo.isChecked() &&
                    qi.listAnswers.get(2).isTrue == radioThree.isChecked() &&
                    qi.listAnswers.get(3).isTrue == radioFour.isChecked()) {
                qi.answeredCorrectly = true;
            }
        }
    }

    /**
     * Calculates the score achieved
     */
    private double calculateResults() {
        double score = 0;
        for (QuestionItem qi : questItems.questList) {
            if (qi.answeredCorrectly) {
                score++;
                Log.i("Test", "correct");
            }
        }
        double dbl = Math.round((score / questItems.questList.size()) * 100);
        Log.i("Test", String.valueOf(dbl));
        return dbl;
    }

    /**
     * Checks which test message fits to the score
     *
     * @return Message to display
     */
    private String getMessageText() {
        double result = calculateResults();
        if (result == 100) {
            return getString(R.string.result_full_score);
        } else if (result > 50) {
            return getString(R.string.result_half_score);
        } else {
            return getString(R.string.result_bad_score);
        }
    }

    /**
     * Handles the dislaying of the coring question and question type.
     * Note: This would not be needed if the GUI was done in code.
     *
     * @param index Index of the current QuestionItem
     */
    public void setActiveQuestion(int index, boolean isPass) {
        QuestionItem qItem = questItems.questList.get(index);
        questionView.setText(qItem.question);
        if (qItem.isMulti) {
            layoutSingle.setVisibility(View.GONE);
            layoutMulti.setVisibility(View.VISIBLE);
            checkOne.setText(qItem.listAnswers.get(0).text);
            if (isPass) {
                if (qItem.listAnswers.get(0).isTrue) {
                    checkOne.setText(checkOne.getText() + " - 87%");
                } else {
                    checkOne.setText(checkOne.getText() + " - 4%");
                }
            }
            checkOne.setChecked(false);
            checkOne.setVisibility(View.VISIBLE);
            checkTwo.setText(qItem.listAnswers.get(1).text);
            if (isPass) {
                if (qItem.listAnswers.get(1).isTrue) {
                    checkTwo.setText(checkTwo.getText() + " - 87%");
                } else {
                    checkTwo.setText(checkTwo.getText() + " - 4%");
                }
            }
            checkTwo.setChecked(false);
            checkTwo.setVisibility(View.VISIBLE);
            checkThree.setText(qItem.listAnswers.get(2).text);
            if (isPass) {
                if (qItem.listAnswers.get(2).isTrue) {
                    checkThree.setText(checkThree.getText() + " - 87%");
                } else {
                    checkThree.setText(checkThree.getText() + " - 4%");
                }
            }
            checkThree.setChecked(false);
            checkThree.setVisibility(View.VISIBLE);
            checkFour.setText(qItem.listAnswers.get(3).text);
            if (isPass) {
                if (qItem.listAnswers.get(3).isTrue) {
                    checkFour.setText(checkFour.getText() + " - 87%");
                } else {
                    checkFour.setText(checkFour.getText() + " - 4%");
                }
            }
            checkFour.setChecked(false);
            checkFour.setVisibility(View.VISIBLE);
        } else {
            layoutSingle.setVisibility(View.VISIBLE);
            layoutMulti.setVisibility(View.GONE);
            radioOne.setText(qItem.listAnswers.get(0).text);
            if (isPass) {
                if (qItem.listAnswers.get(0).isTrue) {
                    radioOne.setText(radioOne.getText() + " - 87%");
                } else {
                    radioOne.setText(radioOne.getText() + " - 4%");
                }
            }
            radioOne.setChecked(false);
            radioOne.setVisibility(View.VISIBLE);
            radioTwo.setText(qItem.listAnswers.get(1).text);
            if (isPass) {
                if (qItem.listAnswers.get(1).isTrue) {
                    radioTwo.setText(radioTwo.getText() + " - 87%");
                } else {
                    radioTwo.setText(radioTwo.getText() + " - 4%");
                }
            }
            radioTwo.setChecked(false);
            radioTwo.setVisibility(View.VISIBLE);
            radioThree.setText(qItem.listAnswers.get(2).text);
            if (isPass) {
                if (qItem.listAnswers.get(2).isTrue) {
                    radioThree.setText(radioThree.getText() + " - 87%");
                } else {
                    radioThree.setText(radioThree.getText() + " - 4%");
                }
            }
            radioThree.setChecked(false);
            radioThree.setVisibility(View.VISIBLE);
            radioFour.setText(qItem.listAnswers.get(3).text);
            if (isPass) {
                if (qItem.listAnswers.get(3).isTrue) {
                    radioFour.setText(radioFour.getText() + " - 87%");
                } else {
                    radioFour.setText(radioFour.getText() + " - 4%");
                }
            }
            radioFour.setChecked(false);
            radioFour.setVisibility(View.VISIBLE);
            layoutSingle.clearCheck();
        }
    }

    /**
     * Displays the Highscore screen via the main activity.
     *
     * @param view View triggering this method
     */
    public void showHighscore(View view) {
        setContentView(R.layout.highscore);
        setHighscorePreferences();
    }

    /**
     * Issues a new Intent to share results
     *
     * @param view View triggering this method
     */
    public void shareResults(View view) {
        String message = String.format(getString(R.string.share_message), Settings.userName, calculateResults(), Settings.quizName);
        createShareIntent(message);
    }

    /**
     * creates the share Intent via a chooser
     *
     * @param textMessage Message to send to the Intent
     */
    private void createShareIntent(String textMessage) {
        // Create the text message with a string
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
        sendIntent.setType("text/plain");

        String title = getString(R.string.share_via);
        Intent chooser = Intent.createChooser(sendIntent, title);

        if (sendIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser);
        }
    }

    /**
     * Saves data to local storage.
     * Needs further research and fine tuning for future projects.
     */
    public void saveData() {
        historyData += "\n" + Settings.userName + " - " + Settings.quizName + " - " + calculateResults() + "%";
        try {
            FileOutputStream fileOut = openFileOutput("gameofquests.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileOut);
            outputWriter.write(historyData);
            outputWriter.close();

            Toast.makeText(getBaseContext(), R.string.score_saved,
                    Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads data from local storage.
     * Needs further research and fine tuning for future projects.
     */
    public String readData() {
        try {
            FileInputStream fileIn = openFileInput("gameofquests.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[100];
            String s = "";
            int charRead;

            while ((charRead = InputRead.read(inputBuffer)) > 0) {
                String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                s += readstring;
            }
            InputRead.close();
            return s;


        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * issues the caller joker
     *
     * @param view
     */
    public void callJoker(View view) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "111-222333444", null)));
        callerButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Passes the question on to the viewers. So, fake viewers in this case.
     * Shows a percentage based chance that any given answer is correct.
     * TODO make this actually random
     *
     * @param view
     */
    public void passOn(View view) {
        setActiveQuestion(questIndex, true);
        passOnButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Removes up to two wrong answers
     * @param view
     */
    public void removeTwo(View view) {
        List<AnswerItem> la = questItems.questList.get(questIndex).listAnswers;
        int count = 0;
        if (questItems.questList.get(questIndex).isMulti) {
            if (!la.get(0).isTrue) {
                checkOne.setVisibility(View.INVISIBLE);
                count++;
            }
            if (!la.get(1).isTrue) {
                checkTwo.setVisibility(View.INVISIBLE);
                count++;
            }
            if (!la.get(2).isTrue && count < 2) {
                checkThree.setVisibility(View.INVISIBLE);
                count++;
            }
            if (!la.get(3).isTrue && count < 2) {
                checkFour.setVisibility(View.INVISIBLE);
            }
        } else {
            if (!la.get(0).isTrue) {
                radioOne.setVisibility(View.INVISIBLE);
                count++;
            }
            if (!la.get(1).isTrue) {
                radioTwo.setVisibility(View.INVISIBLE);
                count++;
            }
            if (!la.get(2).isTrue && count < 2) {
                radioThree.setVisibility(View.INVISIBLE);
                count++;
            }
            if (!la.get(3).isTrue && count < 2) {
                radioFour.setVisibility(View.INVISIBLE);
            }
        }
        removeTwoButton.setVisibility(View.INVISIBLE);
    }
}
