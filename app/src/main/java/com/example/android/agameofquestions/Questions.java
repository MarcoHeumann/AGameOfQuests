package com.example.android.agameofquestions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco on 29.01.2018.
 */

public class Questions {
    List<QuestionItem> questList = new ArrayList<>();
}

class QuestionItem {
    String question;
    boolean isMulti;
    List<AnswerItem> listAnswers = new ArrayList<>();
    boolean answeredCorrectly = false;

    public QuestionItem(String quest, boolean multi, List<AnswerItem> list) {
        this.question = quest;
        this.isMulti = multi;
        this.listAnswers = list;
    }
}

class AnswerItem {
    String text;
    boolean isTrue;

    public AnswerItem(String txt, boolean t) {
        this.text = txt;
        this.isTrue = t;
    }
}